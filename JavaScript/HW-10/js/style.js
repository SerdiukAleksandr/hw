const form = document.querySelector('.password-form');
const input = document.querySelectorAll('[type = password]')
const btn = document.querySelector('.btn')
const warningText = document.createElement('span');
warningText.innerText = 'Нужно ввести одинаковые значения';
warningText.classList.add('warning-text');


form.addEventListener('click', event => {
        if (event.target.tagName === 'I') {
            event.target.classList.toggle('fa-eye-slash');
            event.target.previousElementSibling.type === 'password' ?
                event.target.previousElementSibling.type = 'text' :
                event.target.previousElementSibling.type = 'password';
        }
    })

btn.addEventListener('click', event => {
        form.onsubmit = 'return false'
    event.preventDefault()
        if (input[0].value === input[1].value && input[0].value !== '') {
            warningText.remove()
            alert('You are welcome')
        } else {
            input[1].parentElement.after(warningText)
        }
})
